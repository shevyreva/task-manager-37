package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model) throws Exception;

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception{
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        if (comparator != null ) sql = String.format(sql + " ORDER BY %s", getSortType(comparator));
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception{
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? AND id = ? LIMIT 1", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception{
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? LIMIT ?", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i< index - 1; i++)
                resultSet.next();
            return fetch(resultSet);
        }
    }

    public void removeAll(@NotNull final String userId) throws Exception{
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @NotNull
    public M removeOne(@NotNull final String userId, @NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
            return model;
        }
    }

    @NotNull
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
            @Nullable final M model = findOneById(userId, id);
            if (model == null) throw new ModelNotFoundException();
            removeOne(userId, model);
            return model;
    }

    @NotNull
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception{
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return this.removeOne(userId, model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception{
        @Nullable final M model = findOneById(userId, id);
        return model != null;
    }

    @NotNull
    @Override
    public int getSize(@NotNull final String userId) throws Exception{
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return 0;
            return resultSet.getInt("count");
        }
    }

}
