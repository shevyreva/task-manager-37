package ru.t1.shevyreva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @Override
    public @NotNull User fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD));
        user.setRole(Role.toRole(row.getString(DBConstants.COLUMN_ROLE)));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User add(@NotNull User user){
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOGIN,
                DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_EMAIL, DBConstants.COLUMN_ROLE, DBConstants.COLUMN_LOCKED);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @SneakyThrows
    public User update(@NotNull final User user){
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? " +
                        "WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_LOGIN,
                DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_EMAIL, DBConstants.COLUMN_ROLE,
                DBConstants.COLUMN_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setBoolean(1, user.getLocked());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(),
                DBConstants.COLUMN_LOGIN);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return true;
            else return false;
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(),
                DBConstants.COLUMN_EMAIL);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return true;
            else return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(),
                DBConstants.COLUMN_LOGIN);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return fetch(resultSet);
            else return null;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(),
                DBConstants.COLUMN_EMAIL);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) return fetch(resultSet);
            else return null;
        }
    }

}
