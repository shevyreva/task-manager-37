package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends IService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    M removeOne(@Nullable String userId, @Nullable M model) throws Exception;

    @NotNull
    List<M> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws Exception;

    @NotNull
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    int getSize(@Nullable String userId) throws Exception;

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

}
