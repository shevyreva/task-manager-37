package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    public Connection getConnection();

}
