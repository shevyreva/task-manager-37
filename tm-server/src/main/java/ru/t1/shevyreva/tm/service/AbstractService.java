package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IService;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.IndexEmptyException;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection(){
        return connectionService.getConnection();
    }

    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @SneakyThrows
    public Collection<M> add(@Nullable final Collection<M> models){
        if (models == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(models);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        @Nullable final Collection<M> entities;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entities = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entities;
    }

    @SneakyThrows
    public void clear(){
        @NotNull final Connection connection = getConnection();
        try{
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        }
        catch(Exception e){
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public M add(@Nullable final M model){
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try{
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        }
        catch(Exception e){
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @SneakyThrows
    public M removeOne(@Nullable final M model){
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try{
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeOne(model);
            connection.commit();
        }
        catch(Exception e){
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @SneakyThrows
    public M removeOneById(@Nullable final String id){
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try{
            @NotNull final IRepository<M> repository = getRepository(connection);
            @NotNull M model = repository.removeOneById(id);
            connection.commit();
            return model;
        }
        catch(Exception e){
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @SneakyThrows
    public void removeOneByIndex(@Nullable final Integer index){
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        @NotNull final Connection connection = getConnection();
        try{
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeOneByIndex(index);
            connection.commit();
        }
        catch(Exception e){
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<M> findAll(){
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator){
        if (comparator == null) return findAll();
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id){
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M model =  repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index){
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M model =  repository.findOneByIndex(index);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @SneakyThrows
    public int getSize(){
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @SneakyThrows
    public boolean existsById(final String id){
        try(@NotNull final Connection connection = getConnection()){
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

}
