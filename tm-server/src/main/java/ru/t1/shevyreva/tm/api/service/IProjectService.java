package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project, IProjectRepository> {

    @NotNull
    @SneakyThrows
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    @SneakyThrows
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer Index, @Nullable Status status);

    @NotNull
    @SneakyThrows
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SneakyThrows
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

}
