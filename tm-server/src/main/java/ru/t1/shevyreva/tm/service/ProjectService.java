package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.List;


public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    protected Connection getConnection(){
        return connectionService.getConnection();
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final Connection connection){
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ){
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.add(userId, project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull  Connection connection = getConnection();
        @NotNull final Project project = new Project();
        project.setName(name);
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.add(userId, project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name, final String description
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
            return project;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final ProjectSort sort){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IProjectRepository repository = getRepository(connection);
            return findAll(userId, sort.getComparator());
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

}
