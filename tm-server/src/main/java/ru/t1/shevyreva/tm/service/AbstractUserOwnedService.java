package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IUserOwnedService;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.IndexEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M, R> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull Connection getConnection() {
        return connectionService.getConnection();
    }
    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
             M result = repository.add(userId, model);
             connection.commit();
             return result;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeAll(userId);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final String userId, @Nullable final M model){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            M result = repository.removeOne(userId, model);
            connection.commit();
            return result;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String userId, @Nullable final String id){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            M result = repository.removeOneById(userId, id);
            connection.commit();
            return result;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        @NotNull  Connection connection = getConnection();
        try{
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            M result = repository.removeOneByIndex(userId, index);
            connection.commit();
            return result;
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneById(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneByIndex(userId, index);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try(@NotNull  Connection connection = getConnection()){
            @NotNull IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

}
