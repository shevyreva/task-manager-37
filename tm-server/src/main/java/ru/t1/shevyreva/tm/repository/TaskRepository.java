package ru.t1.shevyreva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @Override
    public @NotNull Task fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull Task task) {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS, DBConstants.COLUMN_CREATED, DBConstants.COLUMN_PROJECT_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, task.getId());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull String userId, @NotNull Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),DBConstants.COLUMN_USER_ID,DBConstants.COLUMN_PROJECT_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

    @NotNull
    @SneakyThrows
    public Task update(@NotNull final Task task){
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
