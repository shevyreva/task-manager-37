package ru.t1.shevyreva.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    @SneakyThrows
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    @SneakyThrows
    public Task add(@NotNull Task task);

    @NotNull
    @SneakyThrows
    public Task add(@NotNull String userId, @NotNull Task task);

    @NotNull
    @SneakyThrows
    public Task update(@NotNull final Task task);

}
