package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.endpoint.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.SessionRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.repository.UserRepository;
import ru.t1.shevyreva.tm.service.*;
import ru.t1.shevyreva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final Connection connection = connectionService.getConnection();
    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.shevyreva.tm.command";
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);
    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(connection);
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(authEndpoint);
        registry(domainEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        //initDemoData();
        loggerService.info("**Welcome to Task Manager Server**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager Server**");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initDemoData() {
        userService.create("user", "user", "user@gmail.com");
        userService.create("test", "test", "test@yandex.ru");
        userService.create("admin", "admin", Role.ADMIN);

        @NotNull final String userTestId = userService.findByLogin("test").getId();
        @NotNull final String userAdminId = userService.findByLogin("admin").getId();

        projectService.add(userTestId, new Project("B PROJECT", Status.COMPLETED));
        projectService.add(userTestId, new Project("A PROJECT", Status.IN_PROGRESS));
        projectService.add(userTestId, new Project("D PROJECT", Status.NON_STARTED));
        projectService.add(userAdminId, new Project("C PROJECT", Status.IN_PROGRESS));

        taskService.add(userTestId, new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(userAdminId, new Task("SUPER TASK", Status.NON_STARTED));
    }

}
