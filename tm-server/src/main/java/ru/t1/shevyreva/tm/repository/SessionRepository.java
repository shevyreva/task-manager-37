package ru.t1.shevyreva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository{

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_SESSION;
    }

    @Override
    @SneakyThrows
    public @NotNull Session fetch(@NotNull ResultSet row){
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstants.COLUMN_ID));
        session.setDate(row.getTimestamp(DBConstants.COLUMN_CREATED));
        session.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        session.setRole(Role.toRole(row.getString(DBConstants.COLUMN_ROLE)));
        return session;

    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull Session session){
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?);",
                getTableName(),
                DBConstants.COLUMN_ID, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @Override
    @SneakyThrows
    public @NotNull Session add(@NotNull String userId, @NotNull Session session){
        session.setUserId(userId);
        return add(session);
    }

    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_CREATED, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}
