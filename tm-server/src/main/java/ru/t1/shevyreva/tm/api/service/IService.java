package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    Collection<M> add(@Nullable Collection<M> models) throws Exception;

    void clear() throws Exception;

    Collection<M> set(@NotNull final Collection<M> models);

    @NotNull
    M add(M model) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @NotNull
    @SneakyThrows
    M findOneById(@Nullable String Id);

    @NotNull
    @SneakyThrows
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model) throws Exception;

    @NotNull
    M removeOneById(@Nullable String Id) throws Exception;

    void removeOneByIndex(@Nullable Integer index) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    int getSize() throws Exception;

}
