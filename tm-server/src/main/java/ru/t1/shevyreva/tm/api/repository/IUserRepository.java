package ru.t1.shevyreva.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @SneakyThrows
    User findByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    User findByEmail(@NotNull String email);

    @NotNull
    @SneakyThrows
    User update(@NotNull final User user);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

/*
    User lockUser(@NotNull final String login);

    User unlockUser(@NotNull final String login);
*/
}
