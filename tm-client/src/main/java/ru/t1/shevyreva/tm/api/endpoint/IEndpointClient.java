package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public interface IEndpointClient {

    void connect();

    void disconnect();

    @NotNull
    Socket getSocket();

    @NotNull
    void setSocket(@NotNull Socket socket);

}
