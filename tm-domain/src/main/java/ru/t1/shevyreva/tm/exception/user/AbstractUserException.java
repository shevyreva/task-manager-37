package ru.t1.shevyreva.tm.exception.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException() {
    }

    public AbstractUserException(@Nullable final String message) {
        super(message);
    }

    public AbstractUserException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
