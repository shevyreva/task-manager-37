package ru.t1.shevyreva.tm.exception.field;

public final class NameEmptyException extends AbsrtactFieldException {

    public NameEmptyException() {
        super("Error! Name is empty!");
    }
}
