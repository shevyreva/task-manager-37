package ru.t1.shevyreva.tm.dto.response.project;

import lombok.Getter;
import lombok.Setter;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

@Setter
@Getter
public final class ProjectClearResponse extends AbstractResponse {
}
